#!/bin/bash
#$ -S /bin/bash
#$ -N Create_DB
#$ -cwd
#$ -l h_vmem=25G
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -m be

echo Running on host: `hostname` >&2
echo Starting on: `date` >&2

# Test de création de base de données Listeria pour prokka
# à partir de téléchargement e genbank sur NCBI

# Handling errors
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation

output="$HOME/Data/Database"
input="$HOME/Data/species.txt"
outdir="$HOME/Data/outdir.txt"
data_dir="$HOME/Data"

cd "$output"
echo 'Go to database repository' >&2

Cronobacter="$output/Cronobacter/"	#--1--#
Escherichia="$output/Escherichia coli/"	#--2--#
Listeria="$output/Listeria monocytogenes/"	#--3--#
Salmonella="$output/Salmonella enterica/"		#--4--#
Staphylococcus="$output/Staphylococcus aureus/"	#--5--#
Shigella="$output/Shigella/"  #--6--#


# Create repository
mkdir -p "$output"

mkdir -p "$Escherichia"
mkdir -p "$Listeria"
mkdir -p "$Salmonella"
mkdir -p "$Shigella"
mkdir -p "$Staphylococcus"

echo " ########### ----------The genomes download started ---------- #########" >&2
# Run downloading

ncbi-genome-download --genus "Cronobacter" --assembly-level complete \
--output-folder $Cronobacter --parallel 5 --flat-output -N -v bacteria

echo 'Cronobacter ---- OK' >&2

ncbi-genome-download --genus "Escherichia coli" --assembly-level complete \
--output-folder $Escherichia --parallel 5 --flat-output -N  -v bacteria

echo 'Escherichia ---- OK' >&2

ncbi-genome-download --genus "Listeria monocytogenes" --assembly-level complete \
--output-folder $Listeria --parallel 5 --flat-output -N -v bacteria

echo 'Listeria ---- OK' >&2

ncbi-genome-download --genus "Salmonella enterica" --assembly-level complete \
--output-folder $Salmonella --parallel 5 --flat-output -N -v bacteria

echo 'Salmonella ---- OK' >&2

ncbi-genome-download --genus "Shigella" --assembly-level complete \
--output-folder $Shigella --parallel 5 --flat-output -N -v bacteria

echo 'Shigella ---- OK' >&2

ncbi-genome-download --genus "Staphylococcus aureus" --assembly-level complete \
--output-folder $Staphylococcus --parallel 5 --flat-output -N -v bacteria

echo 'Staphylococcus ---- OK' >&2

echo 'All Complete Genome genbank have been downloaded.' >&2


#Traitement à faire après le téléchargement de assembly_summary.txt
#pour extraire les liens de téléchargement

##################_________________________________###############
#cat assembly_summary.txt/
#| cut -f12,20 | grep 'Complete Genome' /
#| sed 's/Complete Genome//' > lien.txt
#
# retrait des tabulations au début et à la fin des lignes
#sed 's/^[ \t]*//;s/[ \t]*$//' lien.txt > lien_valid.txt
#
#################___________________________________##############

cd $HOME/Data/Database
##
#input="$HOME/Data/Database/"

#mkdir -p genomes
echo 'Output directory was created' >&2

#output="$HOME/Data/Listeria_monocytogenes/genomes/"

##
#while read lien; do wget -r -nd --no-parent -A 'GCF_*.gbff.gz' $lien/;
#done < $input

echo 'all Complete Genome genbank have been downloaded.' >&2
##
for genome in */*.gz; do gunzip $genome; done

echo 'Unzip .gz files is OK' >&2

##
for f in */*.gbff ; do n=$(basename $f) ; mv $f ${n%%genomic.*}".gb";
done
echo 'extension .gbff to .gb change OK' >&2

##
while read -r line
do
  for gbk in $line/*.gb; do prokka-genbank_to_fasta_db echo -n "$gbk"; done > $line
  makeblastdb -dbtype prot -in $line
done

echo 'Complete Genome database  ------------- is created' >&2

##
#mv Listeria.p* $HOME/.conda/envs/annotation_env/db/genus/
#mv Listeria $HOME/.conda/envs/annotation_env/db/genus/
#mv *.gb $output

echo "Stop job : "`date` >&2



echo "______________________________________________________________________________" >&1
echo "                                                                              " >&1
echo "  1.... ############## ---------- Genomes downloading ---------- #############" >&1
echo " _____________________________________________________________________________" >&1
echo "                                                                              " >&1
## Download assembly_summary_refseq file
#time wget ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt

echo "  " >&1
echo " assembly_summary_refseq.txt ___________ OK" >&1

## Retrieve url for each species

for specie in Cronobacter #Escherichia\ coli Listeria\ monocytogenes Salmonella\ enterica Shigella  Staphylococcus\ aureus
do
  echo "$specie"
  pattern = `echo $specie| sed 's/_/ /'`
  cat assembly_summary_refseq.txt |
  awk -F "\t" -v OFS="\t" '$12=="Complete Genome" \
  {split($1, a, "."); print a[1], $7, $8, $20}' |
  grep "$pattern" | awk 'BEGIN{OFS=FS="/"}{print $0,$NF"_genomic.gbff.gz"}' > "$specie.tsv"

  echo "  " >&1
  echo " $specie.tsv _________ OK " >&1
  # Download genbank file for each specie

  awk 'BEGIN{FS=OFS="\t";filesuffix="genomic.gbff.gz"}\
  {ftpdir=$4;print  ftpdir}' \
  "$specie.tsv" > "$specie.txt"
  echo " $specie.txt __________ OK " >&1
  wget -r -nd -np -P "$specie" -i"$specie.txt"
  echo "$specie download _________ OK"

done

echo " _________________________________________________________________ " >&1
echo "                                                                   " >&1
echo " ________ All 6 species Complete Genome was downloaded  __________ " >&1
echo " _________________________________________________________________ " >&1
echo "                                                                   " >&1


echo "______________________________________________________________________________" >&1
echo "                                                                              " >&1
echo " 2.... ############ ------------- Database creation -------------- ###########" >&1
echo " _____________________________________________________________________________" >&1
echo "                                                                              " >&1


## Unzip downloaded files
for genome in */*.gz
do
  gunzip "$genome"
  echo "Unzip .gz in $genome files is OK" >&2
done
echo " All unzip is _________ OK" >&1

## Change genbank file extension .gbff to .gb


  echo "$specie" >&1
  echo " " >&1
  for gbk in *.gbff
  do n=$(basename $gbk) ; mv $gbk ${n%%genomic.*}".gb"
  done
  echo 'Extension .gbff to .gb change is OK' >&1
