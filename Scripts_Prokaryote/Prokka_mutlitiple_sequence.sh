
##### Example 1 #########
% ls *.fasta
TS1.fasta TS2.fasta TSmut,fasta TScomp.fasta TSwt.fasta

for F in *.fasta; do
  N=$(basename $F .fasta) ;
  prokka --locustag $N --outdir $N --prefix $N  $F ;
done

% ls
TS1.fasta TS2.fasta TSmut.fasta TScomp.fasta TSwt.fasta
TS1/ TS2/ TSmut/ TScomp/ TSwt/

% less TS2/TS2.gbk

###### Example 2 #########
for file in *.fna; do tag=${file%.fna}; 
	prokka --prefix "$tag" --locustag "$tag" --increment 10 --cpus 3 /
	--usegenus --centre XXX --compliant --mincontiglen 200 --genus Escherichia /
	--species coli --strain "$tag" --outdir 2017-06-12_"$tag"_prokka --force /
	--addgenes --gcode 11 "$file";
done

for file in *.fna; do tag=${file%.fna};
	prokka --prefix "$tag" --locustag "$tag" --increment 10 --cpus 3 --usegenus
	--centre XXX --compliant --mincontiglen 200 --genus Escherichia --species coli
	--strain "$tag" --outdir 2017-06-12_"$tag"_prokka --force --addgenes --gcode 11 "$file";
done

####  Example 3 ####
for file1 in *<input file name suffix>; do #(i.e. *.fasta)
	out=${file1%%}; #add output suffix here if desired (i.e._prokka_output)
	prokka file1 --outdir $out;
done

### Crazy Person

prokka \
        --outdir $HOME/genomes/Ec_POO247 --force \
        --prefix Ec_POO247 --addgenes --locustag ECPOOp \
        --increment 10 --gffver 2 --centre CDC  --compliant \
        --genus Escherichia --species coli --strain POO247 --plasmid pECPOO247 \
        --kingdom Bacteria --gcode 11 --usegenus \
        --proteins /opt/prokka/db/trusted/Ecocyc-17.6 \
        --evalue 1e-9 --rfam \
        plasmid-closed.fna


for F in *.fasta; do
  N=$(basename $F .fasta) ;
  prokka --outdir $output/$N --prefix $N \
  --addgenes --kingdom Bacteria --genus Lactococcus \
  --species lactis subsp. lactis --strain IL1403 \
  --centre BIOFORTIS
   --usegenus --compliant --cpus 15  $F ;
done