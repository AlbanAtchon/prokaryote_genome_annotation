#*-------------------------------------*#
#* Script options for the qsub command *#
#*-------------------------------------*#

#!/bin/bash
#$ -S /bin/bash
#$ -N Prokka_annotation
#$ -cwd
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -l h_vmem=25G
#$ -m be

echo Running on host: `hostname` >&1
echo Starting on: `date` >&1
start_time=$(date)

#*------------------------------*#
#* Redirection handling errors  *#
#*------------------------------*#

set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation

#*-----------------------*#
#* Directory definition  *#
#*-----------------------*#

output="$HOME/Results/RGI/Lactococcus_lactis"
db_dir="$HOME/Data/CARD"
#input_prot="$HOME/Data/Proteins_fasta"
input_nuc="$HOME/Data/Contigs/Lactococcus_lactis"
card_json="$HOME/Data/CARD/card.json"
#wildcard="$HOME/Data/CARD/wildcard"
#simulation="$HOME/Data/Resistances_genes.fasta"

mkdir -p "$output"
mkdir -p "$db_dir"

# Activate the conda envs where install the tools 'ARIBA'
#source activate AMR

cd "$db_dir"


#*-------------------*#
#*  Obtain CARD data *#
#*--------------------#

# Download databse
#time wget https://card.mcmaster.ca/latest/data
echo "CARD database was downloaded" >&1

# Database preparation
#tar -xvf data ./card.json
echo "card.json was unzip" >&1
#rgi load --card_json $card_json

#*--------------------------------------------*#
#*  Run RGI with contigs or proteins sequnces *#
#*--------------------------------------------*#

## Run RGI on Lactococcus_lactis Contigs

for contigs in $(ls "$input_nuc"/*.fasta)
do
  name=$(basename $contigs .fasta)
  echo "$name"
  #echo "$contigs"
  #rgi main --input_sequence $contigs --output_file $output/$name --input_type contig  -n 50 --local --clean
  #rgi main --input_sequence $input_prot --output_file $output/$name --input_type protein  -n 50 --local --clean
  #rgi parser -i $output/"$name".json -o $output/"$name"_visu
done
#rgi  heatmap -i $output/ -o $output/heatmap1 -cat gene_family -clus samples -d text
echo "RGI run end ......... "

echo "Start job : $start_time" >&1
echo "Stop job : "`date` >&1
