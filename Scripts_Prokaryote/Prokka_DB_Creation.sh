#*============================================================================================================*#
#*================================================== PROG ID =================================================*#
#*                                                                                                            *#
#* PROJECT..................= PRD20014-OPTIMISATION_WGS                                                       *#
#* ANALYSES.................= Download genbank and create Prokka databases                                    *#
#* PROGRAM NAME.............= script-DDL_DB_Creation_<current_time>.sh                                        *#
#* AUTHOR...................= A. K. ATCHON                                                                    *#
#* WGS-PIPELINE version.....= v1.0                                                                            *#
#* Prokka version...........=  prokka 1.14.6                                                                  *#
#* DEPENDENCIES.............=                                                                                 *#
#*     . BioPerl: Used for input/output of various file formats                                               *#
#*     . Prodigal: Finds protein-codind freatures(CDS)                                                        *#
#*     . BLAST+: Used for similarity searching against protein sequence libraries                             *#
#* INPUT....................= GENBANK files                                                                   *#
#* OUTPUT...................= FASTA files                                                                     *#
#*                                                                                                            *#
#*================================================ DESCRIPTION ===============================================*#
#*                                                                                                            *#
#* OBJECTIVES: Create prokka databases                                                                        *#
#*                                                                                                            *#
#* MAIN STEPS OF THE PROGRAM:                                                                                 *#
#*     . Script options for the qsub command                                                                  *#
#*     . Registration of start time of script execution                                                       *#
#*     . Redirection handling errors                                                                          *#
#*     . Directory definition                                                                                 *#
#*     . Genomes downloading on NBCI by ftp links                                                             *#
#*     . Get the ftp links to the assembly_summary_refseq.txt                                                 *#
#*     . Unzip all downloaded files and change .gff extension to .gb                                          *#
#*     . Generate database for each specie, use Prokka                                                        *#
#*     . Display of start and end times of script execution                                                   *#
#*                                                                                                            *#
#*================================================= REVISIONS ================================================*#
#*---------|-------------|----------------|-------------------------------------------------------------------*#
#* VERSION |     DATE    |     AUTHOR     |                               COMMENTS                            *#
#*---------|-------------|----------------|-------------------------------------------------------------------*#
#* v1.0    |  XX/XX/2020 |    fr_atckok   |                                                                   *#
#*============================================================================================================*#
#*============================================================================================================*#

#*-------------------------------------*#
#* Script options for the qsub command *#
#*-------------------------------------*#

#!/bin/bash
#$ -S /bin/bash# usage1:    cpu=01:31:13, mem=224.40722 GB s, io=15.71834 GB, vmem=54.587M, maxvmem=65.817M
#$ -N DDL_and_Create_DB_1
#$ -cwd
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -l h_vmem=25G
#$ -m be

#*------------------------------------------------*#
#* Registration of start time of script execution *#
#*------------------------------------------------*#

echo Runnin on host: `hostname` >&1
echo Starting on: `date` >&1
start_time=$(date)

#*------------------------------*#
#* Redirection handling errors  *#
#*------------------------------*#

set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation

#*-----------------------*#
#* Directory definition  *#
#*-----------------------*#
output="$HOME/Data/Databases"
prokka_db="$HOME/.conda/envs/annotation_env/db/genus/"
data_dir="$HOME/Data"
mkdir -p "$output"

cd "$output"

#*------------------------------------------*#
#* Genomes downloading on NBCI by ftp links *#
#*------------------------------------------*#
echo "______________________________________________________________________" >&1
echo "                                                                      " >&1
echo " ########### ---------- Genomes downloading ---------- #############  " >&1
echo " _____________________________________________________________________" >&1


## Download assembly_summary_refseq file
time wget ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt

echo "  " >&1
echo " assembly_summary_refseq.txt downloading is OK" >&1


#*-----------------------------------------------------*#
#* Get the ftp links to the assembly_summary_refseq.txt #
#*-----------------------------------------------------*#

for specie in Cronobacter Escherichia_coli Listeria_monocytogenes Salmonella_enterica Shigella Staphylococcus_aureus
do
  echo "$specie"
  pattern=`echo $specie | sed 's/_/ /'`
  cat assembly_summary_refseq.txt |
  awk -F "\t" -v OFS="\t" '$12=="Complete Genome" \
  {split($1, a, "."); print a[1], $7, $8, $20}' |
  grep "$pattern" | awk 'BEGIN{OFS=FS="/"}{print $0,$NF"_genomic.gbff.gz"}' |
  awk 'BEGIN{FS=OFS="\t";filesuffix="genomic.gbff.gz"}\
  {ftpdir=$4;print  ftpdir}' > "$specie.txt"
  echo "$specie.txt is generated OK"

  wget -r -nd -np -P "$specie" -i"$specie.txt"
  echo " ftp links in $specie.txt have been downloaded"

done
echo " ________ All 6 species Complete Genome was downloaded  __________ " >&1


echo "_______________________________________________________________________" >&1
echo "                                                                       " >&1
echo " ############ ------------- Database creation ------------ ########### " >&1
echo " ______________________________________________________________________" >&1


#*------------------------------------------------------------*#
#* Unzip all downloaded files and change .gff extension to .gb #
#*------------------------------------------------------------*#

for genome in */*.gz
do
  gunzip "$genome"
  #echo "Unzip .gz in $genome files is OK" >&2
done
echo " All unzip is _________ OK" >&1

## Change genbank file extension .gbff to .gb
for specie in Cronobacter Shigella Listeria_monocytogenes Escherichia_coli Salmonella_enterica Staphylococcus_aureus
do
  ## extension change for each file on specie folder
  cd $specie

  echo "$specie" >&1
  echo " " >&1
  for gbk in *.gbff
  do n=$(basename $gbk) ; mv $gbk ${n%%genomic.*}".gb"
  done
  echo " " >&1
  echo " $specie Extension .gbff to .gb change is OK" >&1

#*----------------------------------------------*#
#* Generate database for each specie, use Prokka #
#*----------------------------------------------*#

  for file in *.gb
  do
    prokka-genbank_to_fasta_db $file >> $specie.faa
  echo " " >&1
  done
  cd-hit -i $specie.faa -o $specie -T 0 -M 0 -g 1 -s 0.8 -c 0.9
  rm -fv $specie.faa $specie.bak.clstr $specie.clstr
  makeblastdb -dbtype prot -in $specie
  mv $specie.p* $prokka_db
  echo " $specie Prokka Database is created " >&1

  #cp $specie.p* $prokka_db
  #cp $specie $prokka_db
  cd ..
done

echo " Complete Genome databases for 6 speices  ... was created " >&1
echo " "
echo "Start job : $start_time"
echo "Stop job : "`date` >&1

# usage1: cpu=00:01:38, mem=2.29548 GB s, io=10.13850 GB, vmem=24.179M, maxvmem=71.491M
# usage1: cpu=01:31:13, mem=224.40722 GB s, io=15.71834 GB, vmem=54.587M, maxvmem=65.817M
# cpu=00:38:54, mem=820.97853 GB s, io=3.27103 GB, vmem=1.293G, maxvmem=1.293G
