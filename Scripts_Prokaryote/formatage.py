# -*- coding: utf-8 -*-
import re
import os
import glob

# Variable declaration
Locus_tag = {}
others = []
dico ={}


#Preparation of the prokka annotation tsv file
#Deleting the 'ftype' column and duplicate rows
#os.system("cat Cronobacter_subset_1.tsv | sed '/gene/d'| cut -f1,3,4,7 > first_cut.txt")

## Read first file 'first_cut.txt'
# Locu-tag and RefSeq_id extraction

for annot in glob.glob("*.tbl"):
	with open (annot, "r") as file_tbl:

		for li in file_tbl:
			li = li.rstrip('\n')
			locus = re.search("^\t+locus_tag\t(.+)",li)
			refseq = re.search("^\t+inference\tsimilar\sto\sAA\ssequence:RefSeq:(.+)",li)
			if locus :
				loc = locus.group(1)
			if refseq :
				id = refseq.group(1)
				Locus_tag[loc] = id  # Dico with 'locus_tag' for keys and RefSeq_ids in values

## Creating a new file with RefSes_Ids
# Sharing of annotation data with the corresponding RefSeq_id

fichier_ad = open("treatment_1.txt","a")
with open('first_cut.txt','r') as cut_file:

	for line in cut_file:
		line = line.rstrip('\n')
		#print(len(Locus_tag))
		for key, value in Locus_tag.items():
			if line.startswith(key):
				fichier_ad.write(line + '\t' + value)
				fichier_ad.write('\n')
	fichier_ad.close()
print(' First treatement file is created')

## Merge between annotations without RefSeq_Id and those with RefSeq_Id

os.system("cat first_cut.txt treatment_1.txt | uniq -u > output_1.txt")

## Unique retrieval of each annotation
# Dico with locus_tag as key and annotations as values

with open("output_1.txt","r") as f1:
	for li in f1:
		line = li.rstrip('\n')
		locus = re.search("(^\S+)(.+)",line)
		if locus:
			tag = locus.group(1)
			autre = locus.group(2)
			#print(autre)
			dico[tag]=autre
	#print(dico)

## Creating a final annotation file with RefSEq_Ids

fichier_new = open("final.txt","a")
new_line = ""

for key, value in dico.items():
	new_line = str(key) + str(value)
	#print(new_line)
	fichier_new.write(new_line)
	fichier_new.write('\n')
fichier_new.close()

## Adds header to final file
#awk 'NR == 1 {print $0}' *.tsv | cut -f1,3,4,7 > header.txt

fichier_ad2 = open("all_header.txt","a")
with open ("header.txt", "r") as entete:
	for li in entete:
		li = li.rstrip('\n')
		fichier_ad2.write(li + '\t' + 'RefSeq_ID')
		fichier_ad2.write('\n')
fichier_ad2.close()
print(' Final treatement file is created')


#cat all_header.txt final.txt > _annot.txt
