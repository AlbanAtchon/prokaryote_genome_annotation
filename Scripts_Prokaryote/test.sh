#*-------------------------------------*#
#* Script options for the qsub command *#
#*-------------------------------------*#

#!/bin/bash
#$ -S /bin/bash
#$ -N Lacto_DB_and_annotation
#$ -cwd
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -l h_vmem=25G
#$ -m be

#*------------------------------------------------*#
#* Registration of start time of script execution *#
#*------------------------------------------------*#

echo Running on host: `hostname` >&1
echo Starting on: `date` >&1
start_time=$(date)

#*------------------------------*#
#* Redirection handling errors  *#
#*------------------------------*#

set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation


input="$HOME/Data/Contigs"
test_ouput="$HOME/Data/Test"
output="$HOME/Results/Prokka_annotation"
prokka_db="$HOME/.conda/envs/annotation_env/db/genus/"

cd "$test_ouput"

#*-----------------------------------------------------*#
#* Get the ftp links to the assembly_summary_refseq.txt #
#*-----------------------------------------------------*#

for specie in Lactococcus_lactis #Cronobacter Escherichia_coli Listeria_monocytogenes Salmonella_enterica Shigella Staphylococcus_aureus
do
  echo "$specie"
  pattern=`echo $specie | sed 's/_/ /'`
  cat assembly_summary_refseq.txt |
  awk -F "\t" -v OFS="\t" '$12=="Complete Genome" \
  {split($1, a, "."); print a[1], $7, $8, $20}' |
  grep "$pattern" | awk 'BEGIN{OFS=FS="/"}{print $0,$NF"_genomic.gbff.gz"}' |
  awk 'BEGIN{FS=OFS="\t";filesuffix="genomic.gbff.gz"}\
  {ftpdir=$4;print  ftpdir}' > "$specie.txt"
  echo "$specie.txt is generated OK"

  wget -r -nd -np -P "$specie" -i"$specie.txt"
  echo " ftp links in $specie.txt have been downloaded"

done
echo " ________ Lactococcus_lactis Completes Genomes was downloaded  __________ " >&1


echo "_______________________________________________________________________" >&1
echo "                                                                       " >&1
echo " ############ ------------- Database creation ------------ ########### " >&1
echo " ______________________________________________________________________" >&1


#*------------------------------------------------------------*#
#* Unzip all downloaded files and change .gff extension to .gb #
#*------------------------------------------------------------*#

for genome in */*.gz
do
  gunzip "$genome"
  #echo "Unzip .gz in $genome files is OK" >&2
done
echo " All unzip is _________ OK" >&1

## Change genbank file extension .gbff to .gb
for specie in Lactococcus_lactis ##Cronobacter Shigella Listeria_monocytogenes Escherichia_coli Salmonella_enterica Staphylococcus_aureus
do
  cd "$specie"
  echo " " >&1
  for gbk in *.gbff
  do n=$(basename $gbk) ; mv $gbk ${n%%genomic.*}".gb"
  done
  echo " " >&1
  echo " $specie Extension .gbff to .gb change is OK" >&1

#*----------------------------------------------*#
#* Generate database for each specie, use Prokka #
#*----------------------------------------------*#

  for file in *.gb
  do
    prokka-genbank_to_fasta_db $file >> $specie.faa
  done
  cd-hit -i $specie.faa -o $specie -T 0 -M 0 -g 1 -s 0.8 -c 0.9
  rm -fv $specie.faa $specie.bak.clstr $specie.clstr
  makeblastdb -dbtype prot -in $specie
  mv $specie.p* $prokka_db
  echo " $specie Prokka Database is created " >&1
  cd ..
done

echo " Complete Genome databases for Lactococcus_lactis  ... was created " >&1


####### Change the output path before each prokka run  ######
cd "$input"

for specie in  Lactococcus_lactis #Cronobacter Listeria_monocytogenes
do
  cd $specie

  for fasta in  *.fasta
  do
    N=$(basename $fasta .fasta);
    prokka --outdir $output/$specie/$N --force --prefix $N \
    --addgenes --kingdom Bacteria --genus $specie \
    --usegenus --compliant --cpus 15  $fasta
  done
  echo "-------------$specie was annotated-------------------------" >&1
  cd ..
done

echo " "
echo "Start job : $start_time" >&1
echo "Stop job : "`date` >&1
