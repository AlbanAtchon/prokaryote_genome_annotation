# -*- coding: utf-8 -*-
import re
import os
import glob
import pandas as pd

text_1 = "Genes for"
text_2 = "Products"

with open ('All_genes_list.txt',"r") as gene_file:
    col_names = ['Genes','Product','samples']
    df2 = pd.read_csv(gene_file, sep='\t', header=None, names=col_names)

    g = df2.groupby('samples')['Genes'].apply(list).reset_index()
    h = df2.groupby('samples')['Product'].apply(list).reset_index()
    #print(g)
    #print(h)

    df_1 = g.join(pd.get_dummies(g['Genes'].apply(pd.Series).stack()).sum(level=0)).drop('Genes', 1)
    df_2 = h.join(pd.get_dummies(h['Product'].apply(pd.Series).stack()).sum(level=0)).drop('Product', 1)

    DF_1 = df_1.T
    DF_2 = df_2.T

    nbr_genes = len(DF_1) - 1
    nbr_product = len(DF_2) - 1

    #print( nbr_genes ,text_1, nbr_product, text_2)

    #DF_1.to_csv("genes_out.tsv", sep='\t')
    DF_2.to_csv("product_out.tsv", sep='\t')


uniq_dico = {}
product_dico = {}
nw_line = ""

for annot in glob.glob("*_uniq.txt"):
    with open (annot, "r") as file_tbl:
        for li in file_tbl:
            li = li.rstrip('\n')
            li = li.split('\t')
            uniq_dico [li[1]] = li[0]
#print( len(uniq_dico))

new_file = open("Gene_Product.tsv","a")

with open ('product_out.tsv','r') as product:
    for line in product:
        line = line.rstrip('\n')
        lines = line.split('\t')
        #print(line)
        for key, value in uniq_dico.items():
            if not lines[0] != key:
                #print(key,line)
                nw_line = str(value) + '\t' + line
                new_file.write(nw_line)
                new_file.write('\n')
    new_file.close()
