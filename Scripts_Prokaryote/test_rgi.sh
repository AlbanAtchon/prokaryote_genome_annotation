#*-------------------------------------*#
#* Script options for the qsub command *#
#*-------------------------------------*#

#!/bin/bash
#$ -S /bin/bash
#$ -N RGI_simulation
#$ -cwd
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -l h_vmem=25G
#$ -m be

echo Running on host: `hostname` >&1
echo Starting on: `date` >&1
start_time=$(date)

#*------------------------------*#
#* Redirection handling errors  *#
#*------------------------------*#

set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation

#*-----------------------*#
#* Directory definition  *#
#*-----------------------*#
output="$HOME/Results/RGI/Lactococcus_lactis"
db_dir="$HOME/Data/CARD"
#input_prot="$HOME/Data/Proteins_fasta"
input_nuc="$HOME/Data/Contigs/Lactococcus_lactis"
card_json="$HOME/Data/CARD/card.json"
#wildcard="$HOME/Data/CARD/wildcard"
#simulation="$HOME/Data/Resistances_genes.fasta"

mkdir -p "$output"
mkdir -p "$db_dir"

# Activate the conda envs where install the tools 'ARIBA'
#source activate AMR

cd "$db_dir"

#*-------------------*#
#*  Obtain CARD data *#
#*--------------------#

#time wget https://card.mcmaster.ca/latest/data
# "CARD database was downloaded"

#tar -xvf data ./card.json
# "card.json was unzip"
#rgi load --card_json $card_json

### Additional Reference Data for Metagenomics Analyses
#rgi card_annotation -i $card_json > card_annotation.log 2>&1
#rgi load -i $card_json --card_annotation card_database_v3.0.8.fasta

### Obtain WildCARD data

#time wget -O wildcard_data.tar.bz2 https://card.mcmaster.ca/latest/variants
# real	0m26.064s
#user	0m0.911s
#sys	0m1.175s

#mkdir -p wildcard
#tar -xvf wildcard_data.tar.bz2 -C wildcard
#cd wildcard/
#gunzip *.gz
#cd ..

#rgi wildcard_annotation -i wildcard --card_json $card_json -v 3.0.8 > wildcard_annotation.log 2>&1
#rgi load --wildcard_annotation wildcard_database_v3.0.8.fasta \
#--wildcard_index $wildcard/index-for-model-sequences.txt --card_annotation card_database_v3.0.8.fasta

#echo "wildcard_base is OK "

#cd "$input_seq"

echo "RGI run start ......."

#for fasta in $input_seq/*.fasta
#do
#  name=$(basename $fasta)
#  echo "$name"
  #rgi main --input_sequence $fasta --output_file $output/"$name" --input_type protein  -n 50 --local --clean

#ls *.fastq | sed -r 's/_R[12]_001[.]fastq//' | uniq)
for contigs in $(ls "$input_nuc"/*.fasta)
do
  name=$(basename $contigs .fasta)
  echo "$name"
  #echo "$contigs"
  rgi main --input_sequence $contigs --output_file $output/$name --input_type contig  -n 50 --local --clean
  #time rgi tab -i $output/"$name".json
  #time rgi parser -i $output/"$name".json -o $output/"$name"_visu -t protein
done
#cd
echo "RGI run end ......... "

echo "Start job : $start_time" >&1
echo "Stop job : "`date` >&1
