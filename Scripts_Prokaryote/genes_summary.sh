#*-------------------------------------*#
#* Script options for the qsub command *#
#*-------------------------------------*#

#!/bin/bash
#$ -S /bin/bash
#$ -N Prokka_annotation
#$ -cwd
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -l h_vmem=25G
#$ -m be

#*------------------------------------------------*#
#* Registration of start time of script execution *#
#*------------------------------------------------*#

echo Running on host: `hostname` >&1
echo Starting on: `date` >&1
start_time=$(date)

#*------------------------------*#
#* Redirection handling errors  *#
#*------------------------------*#
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation

prokka_dir="$HOME/Results/Prokka_annotation"
Matrix_script="$HOME/Script/Matrice.py"

cd "$prokka_dir"

for specie in Cronobacter Listeria_monocytogenes Lactococcus_lactis
do
  cd $specie
  for annot in  *_annot.txt
  do
    pwd
    echo "$annot"

    cat $annot | sed '1d' | cut -f3 | sed '/^$/d' >> "$specie"_1.txt
    #cat $annot | sed '1d' | cut -f6 | sed '/^$/d' >> "$specie"_2.txt

    cat "$specie"_1.txt | sort | uniq > "$specie"_annot_genes.txt
    #cat "$specie"_2.txt | sort | uniq > "$specie"_annot_product.txt

    sed -i "s/.*/&\t$specie/" "$specie"_annot_genes.txt
    #sed -i "s/.*/&\t$specie/" "$specie"_annot_product.txt

    rm "$specie"_1.txt #"$specie"_2.txt
    #mv "$specie"_annot_genes.txt $prokka_dir/$specie
    #mv "$specie"_annot_product.txt $prokka_dir/$specie

    N=$(basename $annot _annot.txt)
    echo "$N"

    cat $annot | sed '1d' | cut -f3 | sed '/^$/d' > "$N"_list.txt
    sed -i "s/.*/&\t$N/" "$N"_list.txt
    cat "$N"_list.txt >> All_genes_list.txt

    python3 $Matrix_script
    echo ""
  done
  cd ..
  pwd
done
echo "Start job : $start_time" >&1
echo "Stop job : "`date` >&1
