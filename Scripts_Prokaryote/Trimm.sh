#!/bin/sh

#$ -S /bin/bash
#$ -N Quality_Trimming
#$ -cwd
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -m be
#$ -t 1-10

echo Running on host: `hostname` >&2
echo Starting on: `date` >&2

echo '  ________________________________________________________________' >&2
echo ' |                                                               |' >&2
echo ' ####### --------Quality control and trimming ---------- #########' >&2
echo ' |_______________________________________________________________|' >&2

# Handling errors
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation

#Set up whatever package we need to run with
module load java/1.8.0_131 fastqc/0.11.5 trimmomatic/0.39

#Set up the temporary directory
tempodir=/sandbox/users/"$USER"/Trim/"$JOB_ID"
output="$HOME"/results/wgs/qc_trim

#Outputs directories
mkdir -p "$output"
mkdir -p -m 770 "$tempodir"
cd "$tempodir"

#Set up data directory
data_dir="/sandbox/users/aatchon/Data/Fastq_Lactococcus"

#Set up results sub-directories
mkdir -p "$tempodir/fastqc-init"
mkdir -p "$tempodir/fastqc-post"

# Run the program
echo "Run start on $HOSTNAME"`date` >&2

echo "Set up an array with all fastq.gz sequence files..." >&2

tab=($(ls "$data_dir"/*.fastq.gz))
echo "tab = " >&2
printf '%s\n' "${tab[@]}" >&2


echo "Run the First quality control... " >&2
fastqc "${tab[$SGE_TASK_ID]}" -dir $tempodir -o "$tempodir"/fastqc-init

trimmomamic_jar="/sandbox/apps/bioinfo/binaries/trimmomatic/0.39/trimmomatic-0.39.jar"

echo "Run the trimming with trimmomatic tool..." >&2

java -jar "$trimmomamic_jar" PE -phred33 \
"${tab[$SGE_TASK_ID]}" "${tab[$SGE_TASK_ID]}" $(basename "${tab[$SGE_TASK_ID]}" .fastq.gz )_paired.fastq.gz\
$(basename "${tab[$SGE_TASK_ID]}" .fastq.gz )_unpaired.fastq.gz\
$(basename "${tab[$SGE_TASK_ID]}" .fastq.gz )_reverse_paired.fastq.gz\
$(basename "${tab[$SGE_TASK_ID]}" .fastq.gz )_reverse_unpaired.fastq.gz\
SLIDINGWINDOW:4:15 LEADING:3 TRAILING:3 MINLEN:36\

echo "Run the second quality control..." >&2
fastqc $(basename "${tab[$SGE_TASK_ID]}" .fastq.gz )_paired.fastq.gz\
$(basename "${tab[$SGE_TASK_ID]}" .fastq.gz )_reverse_paired.fastq.gz\
 -dir $tempodir -o "$tempodir"/fastqc-post

#Move results in one's directory
mv  "$tempodir" "$output"

echo "Stop job : "`date` >&2
