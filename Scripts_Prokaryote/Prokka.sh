#*============================================================================================================*#
#*================================================== PROG ID =================================================*#
#*                                                                                                            *#
#* PROJECT..................= PRD20014-OPTIMISATION_WGS                                                       *#
#* ANALYSES.................= Prokka genome annotation                                                        *#
#* PROGRAM NAME.............= Prokka_multi_species_annotation_<current_time>.sh                               *#
#* AUTHOR...................= A. K. ATCHON                                                                    *#
#* WGS-PIPELINE version.....= v1.0                                                                            *#
#* Prokka version...........=  prokka 1.14.6                                                                  *#
#* DEPENDENCIES.............=                                                                                 *#
#*     . BioPerl: Used for input/output of various file formats                                               *#
#*     . Prodigal: Finds protein-codind freatures(CDS)                                                        *#
#*     . BLAST+: Used for similarity searching against protein sequence libraries                             *#
#* INPUT....................= FASTA (contigs) files                                                           *#
#* OUTPUT...................= FASTA and GFF3 files                                                            *#
#*                                                                                                            *#
#*================================================ DESCRIPTION ===============================================*#
#*                                                                                                            *#
#* OBJECTIVES: Create prokka databases                                                                        *#
#*                                                                                                            *#
#* MAIN STEPS OF THE PROGRAM:                                                                                 *#
#*     . Script options for the qsub command                                                                  *#
#*     . Registration of start time of script execution                                                       *#
#*     . Redirection handling errors                                                                          *#
#*     . Directory definition                                                                                 *#
#*     . Genomes downloading on NBCI by ftp links                                                             *#
#*     . Get the ftp links to the assembly_summary_refseq.txt                                                 *#
#*     . Unzip all downloaded files and change .gff extension to .gb                                          *#
#*     . Generate database for each specie, use Prokka                                                        *#
#*     . Display of start and end times of script execution                                                   *#
#*                                                                                                            *#
#*================================================= REVISIONS ================================================*#
#*---------|-------------|----------------|-------------------------------------------------------------------*#
#* VERSION |     DATE    |     AUTHOR     |                               COMMENTS                            *#
#*---------|-------------|----------------|-------------------------------------------------------------------*#
#* v1.0    |  XX/XX/2020 |    fr_atckok   |                                                                   *#
#*============================================================================================================*#
#*============================================================================================================*#

#*-------------------------------------*#
#* Script options for the qsub command *#
#*-------------------------------------*#

#!/bin/bash
#$ -S /bin/bash
#$ -N Prokka_annotation
#$ -cwd
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -l h_vmem=25G
#$ -m be

#*------------------------------------------------*#
#* Registration of start time of script execution *#
#*------------------------------------------------*#

echo Running on host: `hostname` >&1
echo Starting on: `date` >&1
start_time=$(date)

#*------------------------------*#
#* Redirection handling errors  *#
#*------------------------------*#

set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation

input="$HOME/Data/Contigs"
output="$HOME/Results/Prokka_annotation"
Format_python="$HOME/Scripts/formatage.py"
prokka_dir="$HOME/Results/Prokka_annotation"
Matrix_script="$HOME/Scripts/Matrice.py"


cd "$input"
#mkdir -p $output
####### Change the output path before each prokka run  ######
echo ""
for specie in Cronobacter Listeria_monocytogenes Lactococcus_lactis
do
  cd $specie
  #pwd
  for fasta in  *.fasta   # selection des contgis au format fasta
  do
    N=$(basename $fasta .fasta);  # Récupération du nom de l'echantillon
    ## Annotation des séquences avec Prokka
    #echo "prokka --outdir $output/$specie/$N --force --prefix $N \
    #--addgenes --kingdom Bacteria --genus $specie \
    #--usegenus --compliant --cpus 15  $fasta"
    #echo "-------------$specie was annotated-------------------------" >&1

    echo "Pretreatment for the creation of new formatted files ......" >&1
    cd "$output/$specie/$N" # Emplacement des répertoires d'annotation de chaque échantillon
    awk 'NR == 1 {print $0}' $N.tsv | cut -f1,3,4,5,6,7 > header.txt # Récupération de l'entête des colonnes à traiter
    cat $N.tsv | sed '/gene/d'| cut -f1,3,4,5,6,7 > first_cut.txt # Récupération des colonnes à traiter
    # cut -f1,3,4,5,6,7 <=> locus_tag, length_bp,	gene,	EC_number,	COG,	product

    echo " The formatting of $N has started ......" >&1
    python3 $Format_python >&1  # script python pour remettre en forme les resultats par échantillon

    echo " Setting up new files " >&1

    cat all_header.txt final.txt > "$N"_an.txt # Rajout le l'entete aux fichiers formatés
    cat "$N"_an.txt | awk -F'\t' 'BEGIN{OFS=FS} $3=="" {$3="unknown"} 1' > "$N"_annot.txt # ajout de 'unknown' dans les colonnes de gènes vides
    mv "$N"_annot.txt $output/$specie # Déplacer les fichiers formatés dans le repertoire de l'espèce
    rm header.txt all_header.txt first_cut.txt output_1.txt treatment_1.txt final.txt "$N"_an.txt # supression des fichiers temporaires

    echo " -----------   $N formatting is complete    ----------------------" >&1
    echo ""
  done

  ### Création de la matrice absence/présence des gènes et products par espèce

  echo "Pretreatment for matrix creation ......" >&1
  cd "$prokka_dir"
  cd $specie
  pwd
  mkdir -p "$specie"_summary

  for annot in  *_annot.txt
  do
    echo " $annot is currently being processed ......" >&1
    cat $annot | sed '1d' | cut -f3,6 | sed '/^$/d' > "$annot"_cut.txt
    cat "$annot"_cut.txt >> "$specie"_1.txt
    cat "$specie"_1.txt | uniq > "$specie"_annot_uniq.txt
    # Fichier "$specie"_annot_uniq.txt correspont aux gènes et aux products unique retrouvé dans chaque échantillon

    N=$(basename $annot _annot.txt)
    echo "$N" >&1
    cat $annot | sed '1d' | cut -f3,6 | sed '/^$/d' > "$N"_list.txt
    # Récupération des gènes et des products dans 2 colonnes differents

    sed -i "s/.*/&\t$N/" "$N"_list.txt # ajout du nom de l'échantillon en troisième colonne
    cat "$N"_list.txt >> All_genes_list.txt # All_genes_list.txt --> tous les genes et products de chaque échantillon
    mv "$annot" "$specie"_summary
    rm "$specie"_1.txt "$annot"_cut.txt "$N"_list.txt

    echo "  The new files have been moved to >>>>>> "$specie"_summary" >&1
  done

  echo ""
  echo "Start matrix creation of $specie ......" >&1
  python3 $Matrix_script >&1

  cat product_out.tsv | awk 'NR == 2 {print $0}'| sed -e "s/samples/Genes\tProduct/g" > entete.txt
  cat entete.txt Gene_Product.tsv >> "$specie"_Matrice.tsv


  mv "$specie"_Matrice.tsv "$specie"_summary
  mv All_genes_list.txt "$specie"_summary
  mv "$specie"_annot_uniq.txt "$specie"_summary
  echo "  The new files have been moved to >>>>>> "$specie"_summary" >&1

  rm entete.txt Gene_Product.tsv product_out.tsv

  echo " ----------   Matrix "$specie"_Matrice.tsv is created   ----------- " >&1
  cd "$input"
done

echo "PROKKA running is finish  ----------- EVERYTHING WENT WELL !!!!! ^^ "
echo ""
echo "Start job : $start_time" >&1
echo "Stop job : "`date` >&1
