#!/ bin/bash

#$ -S /bin/bash
#$ -N Alignment_ClustaW_2
#$ -cwd
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -m be

echo Running on host: `hostname` >&1
echo Starting on: `date` >&1
start_time=$(date)

set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation

input="$HOME/Save_data/Data/Multi_fasta/mini_fasta.fasta"
biginput="$HOME/Save_data/Data/Multi_fasta/Lacto_Multi.fasta"
complet_input="$HOME/Save_data/Data/Multi_fasta/Complet_multi_Lacto.fasta"
data_dir="$HOME/Results/Alignment"

mkdir -p $data_dir

cd "$data_dir"
### Clustalw alignment ###

#clustalo -i $biginput -o Lacto_1.aln --outfmt=clu --force --resno --log=clustalo.log -v --threads=50
clustalw2 -INFILE=$complet_input -ALIGN -TYPE=DNA -OUTFILE=clustalw_all.aln -SEQNOS=ON -SEQNO_RANGE=ON

### MUSCLE alignment ###

#muscle -in $biginput -out muscle.clw -maxiters 1 -diags1 -sv -clw
#muscle -in $input -out muscle.msf -maxiters 1 -diags1 -sv -msf

echo "Start job : $start_time" >&1
echo "Stop job : "`date` >&1
