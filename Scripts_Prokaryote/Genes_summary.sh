#*-------------------------------------*#
#* Script options for the qsub command *#
#*-------------------------------------*#

#!/bin/bash
#$ -S /bin/bash
#$ -N Prokka_annotation
#$ -cwd
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -l h_vmem=25G
#$ -m be

#*------------------------------------------------*#
#* Registration of start time of script execution *#
#*------------------------------------------------*#

echo Running on host: `hostname` >&1
echo Starting on: `date` >&1
start_time=$(date)

#*------------------------------*#
#* Redirection handling errors  *#
#*------------------------------*#
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation

prokka_dir="$HOME/Results/Prokka_annotation"
Matrix_script="$HOME/Scripts/Matrice.py"

cd "$prokka_dir"

for specie in Cronobacter Listeria_monocytogenes Lactococcus_lactis
do
  cd $specie
  mkdir -p "$specie"_summary

  for annot in  *_annot.txt
  do
    pwd
    echo "$annot"

    #cat $annot | sed '1d' | cut -f3,6 | sed '/^$/d' >> "$specie"_1.txt  # details column cut
    cat $annot | sed '1d' | cut -f3,6 | sed '/^$/d' > "$annot"_cut.txt
    cat "$annot"_cut.txt >> "$specie"_1.txt
    #cat $annot | sed '1d' | cut -f6 | sed '/^$/d' > "$specie"_2.txt

    cat "$specie"_1.txt | uniq > "$specie"_annot_uniq.txt
    #sed -i "s/.*/&\t$specie/" "$specie"_annot_uniq.txt # ajout du nom de l'espèce à la fin de chaque ligne --> 2eme  colonne
    rm "$specie"_1.txt "$annot"_cut.txt
    # Fichier "$specie"_annot_uniq.txt correspont aux gènes et aux product unique retrouvé dans chaque échantillon

    N=$(basename $annot _annot.txt)
    echo "$N"
    cat $annot | sed '1d' | cut -f3,6 | sed '/^$/d' > "$N"_list.txt
    # Récupération des gènes et des products dans 2 colonnes

    sed -i "s/.*/&\t$N/" "$N"_list.txt # ajout du nom de l'échantillon en deuxième colonne
    cat "$N"_list.txt >> All_genes_list.txt # All_genes_list.txt --> tous les genes et products de chaque échantillon
    mv "$annot" "$specie"_summary

  done
  python3 $Matrix_script >&1

  cat product_out.tsv | awk 'NR == 2 {print $0}'| sed -e "s/samples/Genes\tProduct/g" > entete.txt
  cat entete.txt Gene_Product.tsv >> "$specie"_Matrice.tsv


  mv "$specie"_Matrice.tsv "$specie"_summary
  mv All_genes_list.txt "$specie"_summary
  mv "$specie"_annot_uniq.txt "$specie"_summary

  rm entete.txt Gene_Product.tsv "$N"_list.txt product_out.tsv
  echo " "$specie"_Matrice.tsv ----------------- is created"

  cd ..
  pwd
done
echo "Start job : $start_time" >&1
echo "Stop job : "`date` >&1
#awk -F "\t" '{ $3=gensub("    ","unknown","G",$3); print}' Cronobacter_subset_1_annot.txt
#awk -F'\t' 'BEGIN{OFS=FS} $3=="" {$3="UNKNOWN"} 1' file
