#!/ bin/bash

#$ -S /bin/bash
#$ -N SNP_find
#$ -cwd
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -m be

echo Running on host: `hostname`
echo Starting on: `date`

echo '  ________________________________________________________________'
echo ' |                                                               |'
echo ' ##### --------- SNP CFSAN pipeline for Find SNPs -------- #######'
echo ' |_______________________________________________________________|'


### variables
INPUT_FASTQ="$HOME/snp_pipeline/Fastq2_Lacto/"
INPUT_REF="$HOME/snp_pipeline/Reference/NC_002662.1.fasta"


# Handling errors

set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation

module load samtools gatk/3.8 bowtie2 bcftools picard
#module list

IFS=$'\n\t'

echo "Run start on $HOSTNAME"`date`

cfsan_snp_pipeline run -m soft -o result_Lacto -s $INPUT_FASTQ $INPUT_REF

# Verify correct results
#cfsan_snp_pipeline data LactoccocusExpectedResults expectedResults

echo "Stop job : "`date`
