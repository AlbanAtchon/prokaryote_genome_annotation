#*============================================================================================================*#
#*================================================== PROG ID =================================================*#
#*                                                                                                            *#
#* PROJECT..................= PRD20014-OPTIMISATION_WGS                                                       *#
#* ANALYSES.................= Download genbank and create Prokka databases                                    *#
#* PROGRAM NAME.............= script-Prokka_DB_Creation_<current_time>.sh                                        *#
#* AUTHOR...................= A. K. ATCHON                                                                    *#
#* WGS-PIPELINE version.....= v1.0                                                                            *#
#* Prokka version...........=  prokka 1.14.6                                                                  *#
#* DEPENDENCIES.............=                                                                                 *#
#*     . BioPerl: Used for input/output of various file formats                                               *#
#*     . Prodigal: Finds protein-codind freatures(CDS)                                                        *#
#*     . BLAST+: Used for similarity searching against protein sequence libraries                             *#
#* INPUT....................= GENBANK files                                                                   *#
#* OUTPUT...................= FASTA files                                                                     *#
#*                                                                                                            *#
#*================================================ DESCRIPTION ===============================================*#
#*                                                                                                            *#
#* OBJECTIVES: Create prokka databases                                                                        *#
#*                                                                                                            *#
#* MAIN STEPS OF THE PROGRAM:                                                                                 *#
#*     . Script options for the qsub command                                                                  *#
#*     . Registration of start time of script execution                                                       *#
#*     . Redirection handling errors                                                                          *#
#*     . Directory definition                                                                                 *#
#*     . Genomes downloading on NBCI by ftp links                                                                                    *#
#*     . Get the ftp links to the assembly_summary_refseq.txt
#*     . Unzip all downloaded files and change .gff extension to .gb                                                     *#
#*     . Generate database for each specie, use Prokka                                                                        *#
#*     . Display of start and end times of script execution                                                   *#
#*                                                                                                            *#
#*================================================= REVISIONS ================================================*#
#*---------|-------------|----------------|-------------------------------------------------------------------*#
#* VERSION |     DATE    |     AUTHOR     |                               COMMENTS                            *#
#*---------|-------------|----------------|-------------------------------------------------------------------*#
#* v1.0    |  XX/XX/2020 |    fr_atckok   |                                                                   *#
#*============================================================================================================*#
#*============================================================================================================*#

#*-------------------------------------*#
#* Script options for the qsub command *#
#*-------------------------------------*#

#!/bin/bash
#$ -S /bin/bash# usage1:    cpu=01:31:13, mem=224.40722 GB s, io=15.71834 GB, vmem=54.587M, maxvmem=65.817M
#$ -N DDL_and_Create_DB_2
#$ -cwd
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -l h_vmem=25G
#$ -m be

#*------------------------------------------------*#
#* Registration of start time of script execution *#
#*------------------------------------------------*#

echo RunninGenerate database for each specie, use Prokkag on host: `hostname` >&1
echo Starting on: `date` >&1
start_time=$(date)

#*------------------------------*#
#* Redirection handling errors  *#
#*------------------------------*#

set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation

#*-----------------------*#
#* Directory definition  *#
#*-----------------------*#
output="$HOME/Data/Databases"
prokka_db="$HOME/.conda/envs/annotation_env/db/genus/"
data_dir="$HOME/Data"
mkdir -p "$output"

cd "$output"

## Change genbank file extension .gbff to .gb
for specie in Cronobacter Shigella Listeria_monocytogenes Escherichia_coli Salmonella_enterica Staphylococcus_aureus
do
  cd $specie
  for file in *.gb
  do
    prokka-genbank_to_fasta_db $file >> $specie.faa
  echo " " >&1
  done
  cd-hit -i $specie.faa -o $specie -T 0 -M 0 -g 1 -s 0.8 -c 0.9
  rm -fv $specie.faa $specie.bak.clstr $specie.clstr
  makeblastdb -dbtype prot -in $specie
  mv $specie.p* $prokka_db
  echo " $specie Prokka Database is created " >&1
  cd ..
done

echo " Complete Genome databases for 6 speices  ... was created " >&1
echo " "
echo "Start job : $start_time"
echo "Stop job : "`date` >&1

# usage1: cpu=00:01:38, mem=2.29548 GB s, io=10.13850 GB, vmem=24.179M, maxvmem=71.491M
# usage1:    cpu=01:31:13, mem=224.40722 GB s, io=15.71834 GB, vmem=54.587M, maxvmem=65.817M
#  cpu=00:24:18, mem=266.35009 GB s, io=2.32382 GB, vmem=22.858M, maxvmem=936.041M
