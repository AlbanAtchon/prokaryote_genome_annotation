#!/ bin/bash

#$ -S /bin/bash
#$ -N Ariba_Lactococcus
#$ -cwd
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -l mem_total=125G
#$ -m be

echo Running on host: `hostname` >&1
echo Starting on: `date` >&1
start_time=$(date)

set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation

# Activate the conda envs where install the tools 'ARIBA'
#source activate AMR

DB_dir="$HOME/Data/ARIBA_DB"
Dbase_name="CARD"
#reads_1="$HOME/Data/Lactococcus/Fastq_Lactococcus/279179_S23_L001_R1_001.fastq"
#reads_2="$HOME/Data/Lactococcus/Fastq_Lactococcus/279179_S23_L001_R2_001.fastq"
output_dir="$HOME/Results/ARIBA/Lactococcus_lactis"
input="$HOME/Data/Lactococcus/Fastq_Lactococcus"
ARIBA_report="$HOME/Results/ARIBA/Lactococcus_lactis/Reports"


## Download reference databases : where reference_name is one of: argannot, card,
##ncbi, megares, plasmidfinder, resfinder, srst2_argannot, vfdb_core, vfdb_full, virulencefinder.
#ariba getref reference_name output_prefix
mkdir -p "$ARIBA_report"
cd "$DB_dir"
#mkdir -p CARD

#time ariba getref card "$Dbase_name"_out # download and created extract file
#real    0m11.649s
#user    0m5.437s
#sys     0m0.495s
echo "The database download is finish" >&1

#Prepare the database for run with "ariba run"
#time ariba prepareref -f "$Dbase_name"_out.fa -m "$Dbase_name"_out.tsv $Dbase_name
#real    0m41.077s
#user    0m37.407s
#sys     0m0.964s

echo "The reference database preparation is finish" >&1
# Move to intput directory where fast.q file is stocked

#cd "$intput"

for file in $(ls "$input"/*.fastq | sed -r 's/_R[12]_001[.]fastq//' | uniq)
do
 # select paired-end reads names
  name=$(basename $file)
  species=$(basename $output_dir)
  #echo "$name"
  time ariba run $Dbase_name "${file}_R1_001.fastq" "${file}_R2_001.fastq" "$output_dir"_"$name"
  echo "Reads $name execution by Ariba is finish" >&1

  cd "$output_dir"_"$name"
  for report in $(ls "$output_dir"_"$name"/report.tsv)
  do
    echo "$report"
    L=$(basename $report .tsv)
    mv "$L".tsv "$L"_"$name".tsv
    mv "$L"_"$name".tsv "$ARIBA_report"
    cd "$DB_dir"
    pwd
  done
done

## 
cd "$ARIBA_report"
ariba summary $species `printf '%s ' *.tsv`

#for all_report in $(ls "$ARIBA_report"/*.tsv)
#do
#  K=$(basename $all_report)
  #printf '%s ' "$K"
#  echo "$K"
  #ariba summary $species "$ARIBA_report"/`printf '%s ' "$K"`
#done

#echo " ariba summary "$species" `printf` "$K" "
#usage 1:  cpu=00:00:56, mem=3.15187 GB s, io=0.84379 GB, vmem=124.905M, maxvmem=131.081M
##ariba run ref reads_1.fq reads_2.fq output_dir # ref is de output directory in prepareref
#time ariba run $Dbase_name $reads_1 $reads_2 $output_dir

echo "Start job : $start_time" >&1
echo "Stop job : "`date` >&1
