#!/ bin/bash

#$ -S /bin/bash
#$ -N SNP_Annot
#$ -cwd
#$ -M kokou.atchon@mxns.com
#$ -V
#$ -m be

echo Running on host: `hostname`
echo Starting on: `date`

echo '  ________________________________________________________________'
echo ' |                                                               |'
echo ' ##### --------- SnpEff pipeline for Annotate SNPs -------- ######'
echo ' |_______________________________________________________________|'

##

OUTPUT=$HOME/snpEff/Lacto_snp_annot
VCF_FILE=$HOME/input_snpma.vcf

echo "      ____________________________________        "
#### Environment variables set

SNPEFF_HOME="/sandbox/users/aatchon/snpEff/" #$SNPEFF_HOME -- path to the installation directory
SNPEFF_JAR="/sandbox/users/aatchon/snpEff/snpEff.jar" #$SNPEFF_JAR -- the snpEff.jar file
SNPSFIT_JAR="/sandbox/users/aatchon/snpEff/SnpSift.jar" #$SNPSIFT_JAR -- the SnpSift.jar file
SNPEFF_CONF="/sandbox/users/aatchon/snpEff/snpEff.config" #$SNPEFF_CONF -- the snpEff config file
SNPEFF_DB="/sandbox/users/aatchon/snpEff/data/"
SPECIES_NAME="Lactococcus_lactis_subsp_lactis_il1403"

mkdir -p $OUTPUT
mkdir -p $SNPEFF_DB
cd $OUTPUT

# Handling errors

set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation

IFS=$'\n\t'

#Set up whatever package we need to run with
module load java/1.8.0_131 snpEff/4.3T

echo "The input ...... $VCF_FILE"
echo "      ____________________________________        "

echo "Run start on $HOSTNAME"`date`

# Download the reference genome
echo " Download database for $SPECIES_NAME"
# The command 'download' does not work on the computing cluster
# so the alternative is download by wget and extract

wget http://downloads.sourceforge.net/project/snpeff/databases/v4_3/snpEff_v4_3_ENSEMBL_BFMPP_32_182.zip

#Extracting file in .zip file to database
unzip snpEff_v4_3_ENSEMBL_BFMPP_32_182.zip -d $SNPEFF_DB
mv  $SNPEFF_DB/home/pcingola/snpEff/data/* $SNPEFF_DB
rm -r $SNPEFF_DB/home/
rm snpEff_v4_3_ENSEMBL_BFMPP_32_182.zip


# If ERROR_CHROMOSOME_NOT_FOUND is posted, do this modification
#cat input.vcf | sed "s/^INPUT_CHR_NAME/SNPEFF_CHR_NAME/" > input_updated_chr.vcf
#cat snpma.vcf | sed "s/^NC_002662.1/Chromosome/" > input_snpma.vcf # in my case

echo "Extrating..... OK and removal .zip file"

java -Xmx10g -jar  $SNPEFF_JAR -c $SNPEFF_CONF -v \
Lactococcus_lactis_subsp_lactis_il1403 $VCF_FILE\
> Lacto_lactis.ann.vcf

#mv snpma.vcf $OUTPUT
#mv SNP_Annot* $OUTPUT

echo "Stop job : "`date`
